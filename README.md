# Role Based Access Control in Angular 16 Applications

The easiest way to understand how access control works is to write your own rgac service. let’s imagine that we have an angular application with users and users have different access levels: `ADMINISTRATOR`, `STAFF`, `USER`. These access levels are defined by roles, so we will have 3 roles:
- `ADMINISTRATOR` - has access to any part of the application at all
- `STAFF` - has access to some part of the administration panel and his account.
- `USER` - only has access to his account.

In Angular applications there are two main places where access should be checked: `routes` and `templates`. For `routes` we use `guards` and for `templates` there is `no standard mechanism`, although it would be possible to check if a user has a certain role via `*ngIf`, but the best option is to write your own `structural directive` and do the checking within it. The `RbacService` work inside the `guards` and `directives`.


